package mapa;

import graficos.Pantalla;
import mapa.cuadro.Cuadro;

public abstract class Mapa {
	protected int ancho;
	protected int alto;

	protected int[] cuadros;
	protected Cuadro[] cuadrosCatalogo;

	public Mapa(int ancho, int alto) {
		this.ancho = ancho;
		this.alto = alto;

		cuadros = new int[ancho * alto];
		generarMapa();
	}

	public Mapa(String ruta) {
		cargarMapa(ruta);
		generarMapa();
	}

	protected void generarMapa() {

	}

	protected void cargarMapa(String ruta) {

	}

	public void actualizar() {

	}

	public void mostrar(final int compensacionX, final int compensacionY,
			final Pantalla pantalla) {
		pantalla.setDiferencia(compensacionX, compensacionY);
		int oeste = compensacionX >> 5; // /32
		int este = (compensacionX + pantalla.getAncho() + Cuadro.LADO) >> 5;
		int norte = compensacionY >> 5;
		int sur = (compensacionY + pantalla.getAlto() + Cuadro.LADO) >> 5;

		for (int y = norte; y < sur; y++) {
			for (int x = oeste; x < este; x++) {
//				 obtenerCuadro(x, y).mostrar(x, y, pantalla);
				if (x < 0 || y < 0 || x >= ancho || y >= alto) {
					Cuadro.VACIO.mostrar(x, y, pantalla);
				} else {
					cuadrosCatalogo[x + y * ancho].mostrar(x, y, pantalla);
				}
			}
		}
	}

	public Cuadro obtenerCuadro(final int x, final int y) {
		if (x < 0 || y < 0 || x >= ancho || y >= alto) {
			return Cuadro.VACIO;
		}
		switch (cuadros[x + y * ancho]) {
		case 0:
			return Cuadro.ASFALTO;
		case 1:
			return Cuadro.ARENA;
		case 2:
			return Cuadro.BORDE_CARRETERA;
		case 3:
			return Cuadro.CENTRO_CARRETERA;
		case 4:
			return Cuadro.ESQUINA_CARRETERA;
		case 5:
			return Cuadro.PARED_PIEDRA;
		case 6:
			return Cuadro.PARED_PIEDRA_INFERIOR;
		case 7:
			return Cuadro.PUERTA_SUPERIOR_IZQUIERDA;
		case 8:
			return Cuadro.PUERTA_INTERMIEDIA_IZQUIERDA;
		case 9:
			return Cuadro.OXIDO;
		case 10:
			return Cuadro.PUERTA_SUPERIOR_IZQUIERDA;
		default:
			return Cuadro.VACIO;
		}

	}
}
