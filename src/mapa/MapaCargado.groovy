package mapa;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import mapa.cuadro.Cuadro;

public class MapaCargado extends Mapa {

	private int[] pixeles;

	public MapaCargado(String ruta) {
		super(ruta);
		// TODO Auto-generated constructor stub
	}

	protected void cargarMapa(String ruta) {
		try {
			BufferedImage imagen = ImageIO.read(MapaCargado.class
					.getResource(ruta));
			ancho = imagen.getWidth();
			alto = imagen.getHeight();

			cuadrosCatalogo = new Cuadro[ancho * alto];
			pixeles = new int[ancho * alto];

			imagen.getRGB(0, 0, ancho, alto, pixeles, 0, ancho);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void generarMapa() {
		for (int i = 0; i < pixeles.length; i++) {
			println pixeles[i]
			switch (pixeles[i]) {
			case 0xff000000:
				cuadrosCatalogo[i] = Cuadro.ASFALTO;
				continue;
			case 0xffffe27b:
				cuadrosCatalogo[i] = Cuadro.ARENA;
				continue;
			case 0xfff4c008:
				cuadrosCatalogo[i] = Cuadro.BORDE_CARRETERA;
				continue;
			case 0xffbfbcb2:
				cuadrosCatalogo[i] = Cuadro.CENTRO_CARRETERA;
				continue;
			case 0xff939dd4:
				cuadrosCatalogo[i] = Cuadro.ESQUINA_CARRETERA;
				continue;
			case 0xff7dc9de:
				cuadrosCatalogo[i] = Cuadro.PARED_PIEDRA;
				continue;
			case 0xff7e7235:
				cuadrosCatalogo[i] = Cuadro.PARED_PIEDRA_INFERIOR;
				continue;
			case 0xff732f00:
				cuadrosCatalogo[i] = Cuadro.PUERTA_SUPERIOR_IZQUIERDA;
				continue;
			case 0xff97531c:
				cuadrosCatalogo[i] = Cuadro.PUERTA_INTERMIEDIA_IZQUIERDA;
				continue;
			case 0xff53971d:
				cuadrosCatalogo[i] = Cuadro.PUERTA_SUPERIOR_CENTRAL;
				continue;
			case 0xff971d2a:
				cuadrosCatalogo[i] = Cuadro.OXIDO;
				continue;
			default:
				cuadrosCatalogo[i] = Cuadro.VACIO;
			}
		}
	}

}
